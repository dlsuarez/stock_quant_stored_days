# -*- coding: utf-8 -*-
###############################################################################
#    License, author and contributors information in:                         #
#    __openerp__.py file at the root folder of this module.                   #
###############################################################################

from openerp import models, fields, api
from openerp.tools.translate import _
from openerp.exceptions import Warning
from datetime import datetime, date, time, timedelta
import logging

class stock_quant(models.Model):

    _inherit = 'stock.quant'

    _logger = logging.getLogger(__name__)

    stored_days = fields.Integer(
        string=_('Stored days'),
        required=False,
        readonly=True,
        compute=lambda self: self._compute_stored_fields(),
        help=_('Number of days stored')
    )

    out_date = fields.Datetime(
        string=_('Departure date'),
        required=False,
        readonly=True,
        index=False,
        default=fields.datetime.now(),
        compute=lambda self: self._compute_out_date(),
        help=_('Departure date of quant move')
    )

    @api.one
    def _compute_stored_fields(self):
        in_date = fields.Datetime.from_string(self.in_date)
        if self.location_id.usage == "internal":
            delta = datetime.today() - in_date
        else:
            out_date = fields.Datetime.from_string(self.out_date)
            delta = out_date - in_date
        self.stored_days = delta.days + 1

    @api.multi
    def _compute_out_date(self):
        for record in self:
            out_date = datetime.strptime("0001-01-01 00:00:00", "%Y-%m-%d %H:%M:%S")
            for move in record.history_ids:
                if record.location_id.usage != "internal":
                    if datetime.strptime(move.date, "%Y-%m-%d %H:%M:%S") > out_date:
                        out_date = datetime.strptime(move.date, "%Y-%m-%d %H:%M:%S")
                else:
                    out_date = False
            record.out_date = out_date
